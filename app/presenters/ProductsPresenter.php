<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 18. 10. 2016
 * Time: 23:09
 */

namespace App\Presenters;

use Nette;
use Tracy\Debugger;


class ProductsPresenter extends Nette\Application\UI\Presenter
{
    private $database;


    /**
     * ProductsPresenter constructor.
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderList()
    {

        $products = $this->database->table('product');

        $categories = $this->database->table('category');

        $categories = iterator_to_array($categories);
        $products = iterator_to_array($products);
        echo $this->listProducts($categories, $products, null);

    }

    /**
     * @param $categories
     * @param $level
     * @return string
     */
    private function listProducts($categories, $products, $level)
    {


        $text = "<ul>";
        foreach ($categories as $category) {

            $b = $category->parent_category_id == $level;
            if ($b) {

                $text = $text . "<li>";
                $text = $text . $category->category_name;

                foreach ($products as $product) {
                    if ($product->category_id == $category->id) {
                        $text .= " ".$product->product_name;
                    }
                }

                $text = $text . $this->listProducts($categories, $products, $category->id);
                $text = $text . "</li>";
            }
        }
        $text = $text . "</ul>";

        return $text;
    }




}